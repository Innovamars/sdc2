#!/usr/bin/env python
# -*- coding: utf-8 -*-

palabra = input("Hola, en que te puedo ayudar? ")
from chatterbot import ChatBot
chatbot = ChatBot("Ron Obvious")

from chatterbot.trainers import ListTrainer

conversation = [
    "Hola",
     "¡Hola!",
     "¿Como estas?",
     "Lo estoy haciendo genial.",
     "Es bueno oír eso",
     "Gracias.",
     "De nada."
]

trainer = ListTrainer(chatbot)

trainer.train(conversation)

response = chatbot.get_response(palabra)
print(response)
